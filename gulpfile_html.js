module.exports = function (gulp, plugins, getFolders) {
  function onError(e) {
    console.log(e.toString());
    this.emit('end');
  }

  /* Paths */
  var src = {
      components: 'src/components'
    },
    dist = {
      components: 'dist'
    };

  var optionsHtml = {
    indent_with_tabs: true,
    max_preserve_newlines: 0,
    indentSize: 2,
    unformatted: ['abbr', 'area', 'b', 'bdi', 'bdo', 'br', 'cite', 'code', 'data', 'datalist', 'del', 'dfn', 'em', 'embed', 'i', 'ins', 'kbd', 'keygen', 'map', 'mark', 'math', 'meter', 'noscript', 'object', 'output', 'progress', 'q', 'ruby', 's', 'samp', 'small', 'strong', 'sub', 'sup', 'template', 'time', 'u', 'var', 'wbr', 'text', 'acronym', 'address', 'big', 'dt', 'ins', 'strike', 'tt']
  };
  /* End Paths */

  var replOption = {
    del: ['#color-skin'],
    mod: ['#skin', '#bootstrap']
  };

  return function () {

    var components = getFolders(src.components).map(function (folder) {
      return gulp.src(plugins.path.join(src.components, folder, '/index.html'))
        .pipe(plugins.plumber({errorHandler: onError}))
        .pipe(plugins.fileInclude())
        .pipe(plugins.concat(folder + '.html'))
        //.pipe(plugins.repl(replOption))
        .pipe(plugins.htmlmin({collapseWhitespace: true, removeComments: true}))
        .pipe(plugins.htmlBeautify(optionsHtml))
        .pipe(plugins.changedInPlace({firstPass: true}))
        .pipe(gulp.dest(dist.components));
    });

    return plugins.merge(components);

  };
};