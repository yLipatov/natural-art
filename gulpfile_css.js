module.exports = function (gulp, plugins, getFolders, mobileSettings) {
  function onError(e) {
    console.log(e.toString());
    this.emit('end');
  }

  /* Paths */
  var src = {
      main: 'src/scss/[^_]*.scss',
      components: 'src/components'
    },
    dist = {
      main: 'dist/css',
      components: 'dist/css/components'
    };
  /* End Paths */

  return function () {

    gulp.src(src.main)
      .pipe(plugins.plumber({errorHandler: onError}))
      .pipe(plugins.if(!mobileSettings, plugins.sourcemaps.init()))
      .pipe(plugins.sass())
      .pipe(plugins.autoprefixer({browsers: ['defaults', 'ios 6']}))
      .pipe(plugins.if(mobileSettings, plugins.combineMq({beautify: false})))
      .pipe(plugins.changedInPlace({firstPass: true}))
      .pipe(gulp.dest(dist.main))
      .pipe(plugins.csso())
      .pipe(plugins.rename({suffix: '.min'}))
      .pipe(plugins.if(!mobileSettings, plugins.sourcemaps.write('./maps')))
      //.pipe(plugins.debug({title: 'CSS:'}))
      .pipe(gulp.dest(dist.main));

    var components = getFolders(src.components).map(function (folder) {
      return gulp.src(plugins.path.join(src.components, folder, '/*.scss'))
        .pipe(plugins.plumber({errorHandler: onError}))
        .pipe(plugins.concat(folder + '.scss'))
        .pipe(plugins.if(!mobileSettings, plugins.sourcemaps.init()))
        .pipe(plugins.sass())
        .pipe(plugins.autoprefixer({browsers: ['defaults', 'ios 6']}))
        .pipe(plugins.if(mobileSettings, plugins.combineMq({beautify: false})))
        .pipe(plugins.changedInPlace({firstPass: true}))
        .pipe(gulp.dest(dist.components))
        .pipe(plugins.csso())
        .pipe(plugins.rename({suffix: '.min'}))
        .pipe(plugins.if(!mobileSettings, plugins.sourcemaps.write('./maps')))
        //.pipe(plugins.debug({title: 'CSS:'}))
        .pipe(gulp.dest(dist.components));
    });

    return plugins.merge(components);

  }
};