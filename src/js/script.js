(function($){

	var mobile = $('.menu'),
			click = $('.menu-mobile');

	click.on('click',	function () {
		mobile.slideToggle('');
	});


	var mySwiper = new Swiper ('.swiper-container', {
		slidesPerView: 4,
		loop: true,
		breakpoints: {
			991: {
				slidesPerView: 1
			},
			768: {
				slidesPerView: 1
			},
			640: {
				slidesPerView: 1
			},
			320: {
				slidesPerView: 1
			}
		}
	})

})(jQuery);