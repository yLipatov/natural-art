module.exports = function (gulp, plugins) {
  /* Paths */
  var src = {
      main: ['src/img/**/*.{jpg,jpeg,png,gif}', '!src/img/sprite/*.*']
    },
    dist = {
      main: 'dist/img'
    };
  /* End Paths */

  return function () {

    gulp.src(src.main)
      .pipe(plugins.changed(dist.main))
      .pipe(plugins.imagemin([
        plugins.imagemin.gifsicle({interlaced: true}),
        plugins.imagemin.jpegtran({progressive: true}),
        plugins.imageminJpegRecompress({
          progressive: true,
          max: 80,
          min: 70
        }),
        plugins.imagemin.svgo({plugins: [{removeViewBox: true}]}),
        plugins.imagemin.optipng({optimizationLevel: 5}),
        plugins.pngquant({quality: "65-70", speed: 5})
      ]))
      .pipe(gulp.dest(dist.main));

  };
};