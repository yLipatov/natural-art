'use strict';

var browserSync = require('browser-sync').create(),
    gulp        = require('gulp'),
    plugins     = require('gulp-load-plugins')();

plugins.fs                      = require('fs');
plugins.path                    = require('path');
plugins.merge                   = require('merge-stream');
plugins.imageminJpegRecompress  = require('imagemin-jpeg-recompress');
plugins.pngquant                = require('imagemin-pngquant');

plugins.repl = require('./repl');


gulp.task('pl', function () {
  console.log(plugins);
});

/*
gulp-load-plugins:
  (+)autoprefixer
  (+)changed
  (+)changedInPlace
  (+)clean
  (+)combineMq
  (+)concat
  (+)csso
  (+)debug
  (+)fileInclude
  (+)htmlBeautify
  (+)htmlmin
  (+)if
  (+)imagemin
  (+)plumber
  (+)rename
  (+)sass
  (+)sequence
  (+)sourcemaps
  (+)uglify
  (+)watch
  (+)spritesmith
  (+)imagemin-jpeg-recompress
  (+)imagemin-pngquant
  (+)fs
  (+)path
  (+)merge
  (+)repl
*/

/* Secondary functions */
var getFolders = function (dir) {
  return plugins.fs.readdirSync(dir)
    .filter(function (file) {
      return plugins.fs.statSync(plugins.path.join(dir, file)).isDirectory();
    });
};
/* End Secondary functions */

/* Paths and variables */
var src = {
    main: 'src',
    scss: 'src/scss',
    js: 'src/js',
    components: 'src/components',
    templates: 'src/templates',
    img: 'src/img'
  },
  dist = {
    main: 'dist',
    css: 'dist/css',
    js: 'dist/js',
    img: 'dist/img'
  },
  mobileSettings = false;
/* End Paths and variables */

/* Service tasks */
gulp.task('clean-css-maps', function () {
  return gulp.src([dist.css + '/maps', dist.css + '/components/maps'], {read: false})
    .pipe(plugins.if(mobileSettings, plugins.clean()))
});
gulp.task('clean-dist', function () {
  console.log('Clean dist!')
});
/* End Service tasks */


gulp.task('scss', require('./gulpfile_css')(gulp, plugins, getFolders, mobileSettings));
gulp.task('js', require('./gulpfile_js')(gulp, plugins, getFolders));
gulp.task('html', require('./gulpfile_html')(gulp, plugins, getFolders));
gulp.task('vendor', require('./gulpfile_vendor')(gulp, plugins));
gulp.task('img', require('./gulpfile_img')(gulp, plugins));
gulp.task('sprite', require('./gulpfile_sprite')(gulp, plugins));

gulp.task('browser-sync', function () {
  browserSync.init({
    server: {
      baseDir: dist.main
    }
  });

  browserSync.watch(dist.main + '/*.html').on('change', browserSync.reload);
  browserSync.watch([dist.css + '/*.css', dist.css + '/components/*.css'], function (event, file) {
    if (event === 'change') browserSync.reload();
  });
  browserSync.watch([dist.js + '/*.js', dist.js + '/components/*.js'], function (event, file) {
    if (event === 'change') browserSync.reload();
  });
});

gulp.task('watch', function () {
  gulp.watch([src.scss + '/**/*.scss', src.components + '/**/*.scss'], {usePolling: true}, ['scss']);
  gulp.watch([src.js + '/**/*.js', src.components + '/**/*.js'], ['js']);
  gulp.watch([src.components + '/**/*.html', src.templates + '/**/*.html'], ['html']);
  gulp.watch([src.img + '/**/*.{jpg,jpeg,png,gif}', '!src/img/sprite/*.*'], ['img']);
  gulp.watch(src.img + '/sprite/*.*', ['sprite']);
});

gulp.task('default', ['clean-dist'], plugins.sequence('vendor', 'clean-css-maps', 'scss', 'js', 'html', 'img', 'sprite', ['browser-sync', 'watch']));
