'use strict';

const { Transform } = require('readable-stream'),
      cheerio       = require('cheerio');


module.exports = function repl(options) {
  options = options || {};

  return new Transform({
    objectMode: true,
    transform: function replTransform(file, enc, cb) {
      if (file.isNull()) {
        cb(null, file);
        return;
      }

      if (file.isStream()) {
        console.log('Streaming not supported');
        cb(null, file);
        return;
      }

      if (file.isBuffer()) {
        const data = file.contents.toString();
        const $ = cheerio.load(data);

        if(options.del !== undefined) {
          options.del.forEach(function(item, i, arr) {
            $(item).remove()
          });
        }

        if(options.mod !== undefined) {
          options.mod.forEach(function(item, i, arr) {
            let itemTag       = $(item),
                itemHref      = itemTag.attr('href'),
                dataHtmlSkin  = $('html').data('skin').replace(/\b.css\b/g, ''),
                itemHrefNew   = itemHref.replace(/^\bcss\/\b/g, 'css/' + dataHtmlSkin + '/');

            itemTag.attr('href', itemHrefNew);
          });
        }

        file.contents = new Buffer($.html());
        return cb(null, file);
      }

    }
  })
};